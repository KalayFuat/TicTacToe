package com.example.tictactoe;

import static com.example.tictactoe.GlobalDefines.TAG;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;


public class GameScores extends ConstraintLayout implements GameController.IGameEnded,
                                                            GameController.INewGameStarted{

    GameState lastGameResult;

    public GameScores(Context context) {
        super(context);

        init();
    }

    public GameScores(Context context, AttributeSet attrs) {
        super(context, attrs);

        init();
    }

    private void init(){
    }

    public void displayScores(GameState gameState){
        ((TextView)findViewById(R.id.textPlayerOScore)).setText(String.valueOf(gameState.playerOScore));
        ((TextView)findViewById(R.id.textPlayerXScore)).setText(String.valueOf(gameState.playerXScore));
    }

    @Override
    public void onNewGameStarted(GameState gameState) {
        this.setVisibility(VISIBLE);
        lastGameResult = gameState;
        displayScores(gameState);
    }

    @Override
    public void onFirstTimeLoad(GameState gameState) {
        this.setVisibility(INVISIBLE);
    }

    @Override
    public void gameEnded(GameState gameState) {
        lastGameResult = gameState;
        displayScores(gameState);
    }

    @Override
    public void roundEnded(GameState gameState) {
        Log.i(TAG, "GameScores:roundEnded: ");

        lastGameResult = gameState;
        displayScores(gameState);
    }

    @Override
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if (lastGameResult.lastRoundResult.result == GameGlobals.GAME_ENDED_O_WON) {
            //((TextView)findViewById(R.id.textPlayerOScore)).setText(String.valueOf(lastGameResult.playerOScore));
        } else if (lastGameResult.lastRoundResult.result == GameGlobals.GAME_ENDED_X_WON) {
            //((TextView)findViewById(R.id.textPlayerXScore)).setText(String.valueOf(lastGameResult.playerXScore));
        }
    }


}
