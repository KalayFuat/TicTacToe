package com.example.tictactoe;

import android.content.Intent;
import android.graphics.Rect;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ToggleButton;
import android.widget.Toast;

import static com.example.tictactoe.GameGlobals.DEFAULT_AI_DIFFICULTY;
import static com.example.tictactoe.GameGlobals.DEFAULT_GRID_SIZE;
import static com.example.tictactoe.GameGlobals.DEFAULT_VOLUME_BUTTON_STATE;
import static com.example.tictactoe.GameGlobals.DEFAULT_WIN_COMBO;
import static com.example.tictactoe.SettingsActivity.TAG_AI_DIFFICULTY;
import static com.example.tictactoe.SettingsActivity.TAG_GRID_SIZE;
import static com.example.tictactoe.SettingsActivity.TAG_VOLUME;
import static com.example.tictactoe.SettingsActivity.TAG_WIN_COMBO;

//TODO: when home button is pressed from GameActivity and game is opened again, it starts from
// MainActivity activity and the game state is not lost and continues when user presses
// 'Single Player'.
// fix: when user exits from GameActivity, set a flag or save state, then have
// MainActivity check it open starting up and start GameActivity upon loading..

public class GameActivity extends AppCompatActivity implements
        GameView.IMainViewInflated,
        GameController.IUndoCountChanged{

    public static final int SETTINGS_REQUEST = 1;

    private int adjustedGridSize = DEFAULT_GRID_SIZE;
    private int adjustedWinCombo = DEFAULT_WIN_COMBO;
    private int adjustedAI = DEFAULT_AI_DIFFICULTY;
    private int adjustedVolume = DEFAULT_VOLUME_BUTTON_STATE;

    //TODO: these should go into the GameView class
    private PlayerTurnFrame playerTurnFrame;
    private WinningComboSizeView winningComboSizeView;
    private TictactoeGrid gameGrid;
    private PlayerTurnDots playerTurnDots;
    private BackgroundAux backgroundAux;
    private SoundEffects sounds;
    private GameScores scores;
    private GameResultDisplay gameResultDisplay;

    private GameView gameView;
    private GameModel gameModel;
    private GameController gameController;
    private GameSession gameSession;
    private TictactoeAI gameAI;

    private int colour;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sounds = new SoundEffects(this);
        gameModel = new GameModel();
        gameController = new GameController();
        gameSession = new GameSession();
        gameAI = new TictactoeAI();

        colour = ContextCompat.getColor(getApplicationContext(), R.color.colorCellBlank);

        Utilities.requestFullScreen(this);
        Utilities.requestPortrait(this);

        ActionBar bar = getSupportActionBar();
        if(bar != null)
            bar.hide();

        setContentView(R.layout.activity_game);

        if (getIntent().getExtras() != null)
        {
            Bundle bundle = this.getIntent().getExtras();

            adjustedGridSize = bundle.getInt(TAG_GRID_SIZE);
            adjustedWinCombo = bundle.getInt(TAG_WIN_COMBO);
            adjustedAI = bundle.getInt(TAG_AI_DIFFICULTY);
            adjustedVolume = bundle.getInt(TAG_VOLUME);
        }
        else{
            adjustedGridSize = DEFAULT_GRID_SIZE;
            adjustedWinCombo = DEFAULT_WIN_COMBO;
            adjustedAI = DEFAULT_AI_DIFFICULTY;
            adjustedVolume = DEFAULT_VOLUME_BUTTON_STATE;
        }

        gameView = findViewById(R.id.gameContainer);
        playerTurnFrame = findViewById(R.id.playerTurnFrame);
        playerTurnDots = findViewById(R.id.playerTurnDots);
        winningComboSizeView = findViewById(R.id.textViewWinComboCount);
        gameGrid = findViewById(R.id.gameGrid);
        backgroundAux = findViewById(R.id.viewBackgroundAux);
        scores = findViewById(R.id.playerScoresDisplay);
        gameResultDisplay = findViewById(R.id.bottomSectionContainer);

        gameView.setViewInflatedCallback(this);

        //model-controller bind
        gameController.setModel(gameModel);

        //view-controller bind
        gameGrid.setController(gameController);

        //new game started
        gameController.addInewGameStarted(gameGrid);
        gameController.addInewGameStarted(sounds);
        gameController.addInewGameStarted(gameSession);
        gameController.addInewGameStarted(gameView);
        gameController.addInewGameStarted(gameAI);
        gameController.addInewGameStarted(scores);
        gameController.addInewGameStarted(gameResultDisplay);
        gameController.addInewGameStarted(winningComboSizeView);

        //new round started
        gameController.addInewRoundStarted(gameGrid);
        gameController.addInewRoundStarted(sounds);
        gameController.addInewRoundStarted(gameView);
        gameController.addInewRoundStarted(gameResultDisplay);

        //touch/cell inputs
        gameController.addIcellInteraction(gameGrid);
        gameController.addIcellInteraction(sounds);
        gameController.addIcellInteraction(gameSession);

        //game ended
        gameController.addIgameEndedCelebration(sounds);
        gameController.addIgameEndedCelebration(gameGrid);
        gameController.addIgameEndedCelebration(playerTurnDots);
        gameController.addIgameEndedCelebration(scores);
        gameController.addIgameEndedCelebration(gameView);
        gameController.addIgameEndedCelebration((gameResultDisplay));

        //display for number-of-match-in-a-row
        gameController.addIwinComboCount(winningComboSizeView);

        //player turn changed
        gameController.addIplayerTurn(playerTurnFrame);
        gameController.addIplayerTurn(playerTurnDots);

        //undo button pressed
        gameController.addIundoCountChanged(this);

        //game is quitting
        gameController.addIgameIsQuitting(sounds);

        //game is paused
        gameController.addIgamePauseResume(sounds);

        //set AI-Active state based on ai-toggle-button state
        gameController.setAiActive( ((ToggleButton)findViewById(R.id.buttonAISwitch)).isChecked() );

       // playerTurnDots.setLowDPImode(0);
        //winningComboSizeView.setLowDPImode(0);
    }

    public void volumeButtonClicked(View view, int volume){
        //View view = findViewById(R.id.buttonVolume);
        view.setSelected(!view.isSelected());
        adjustedVolume = view.isSelected() ? 1 : 0;

        if(view.isSelected())
            sounds.pauseSounds();
        else
            sounds.continuePlaying();

        //Toast.makeText(this, String.valueOf(findViewById(R.id.buttonVolume).isSelected()), Toast.LENGTH_SHORT).show();
    }

    public void onClick_Continue(View view) {
        ButtonGuide btn = findViewById(R.id.buttonNextRound);
        gameController.ContinueButton(btn.getText().toString());
    }

    public void onClick_UndoMove(View view){
        gameController.onClick_Undo();
    }

    public void onClick_Volume(View view){
        volumeButtonClicked(view, adjustedVolume);
    }

    public void onClick_ToggleAI(View view){
        gameController.setAiActive(((ToggleButton)view).isChecked());
    }

    public void onClick_Settings(View view){
        Intent intent = new Intent(this, SettingsActivity.class);
        Bundle bundle = new Bundle();

        bundle.putInt(SettingsActivity.TAG_GRID_SIZE, adjustedGridSize);
        bundle.putInt(SettingsActivity.TAG_WIN_COMBO, adjustedWinCombo);
        bundle.putInt(SettingsActivity.TAG_AI_DIFFICULTY, adjustedAI);
        bundle.putInt(SettingsActivity.TAG_VOLUME, adjustedVolume);

        intent.putExtras(bundle);
        startActivityForResult(intent, SETTINGS_REQUEST);
    }

    public void onClick_Reset(View view){
        gameController.startNewGame();
    }

    public void onClick_Back(View view){
        finish();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume(){
        super.onResume();

        gameController.resumeGame();

        View view = findViewById(R.id.buttonVolume);
        if(view != null) {
            if (view.isSelected())
                sounds.pauseSounds();
            else
                sounds.continuePlaying();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        gameController.pauseGame();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        gameController.exitGame();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == SETTINGS_REQUEST){
            if(resultCode == RESULT_OK){
                Bundle bundle = data.getExtras();

                int gridSize = bundle.getInt(SettingsActivity.TAG_GRID_SIZE);
                int winCombo = bundle.getInt(SettingsActivity.TAG_WIN_COMBO);
                int ai = bundle.getInt(SettingsActivity.TAG_AI_DIFFICULTY);
                int volume = bundle.getInt(TAG_VOLUME);

                if(volume != adjustedVolume){
                    adjustedVolume = volume;
                    volumeButtonClicked(findViewById(R.id.buttonVolume), adjustedVolume);
                }

                if(adjustedGridSize != gridSize || adjustedWinCombo != winCombo) {

                    //TODO: tell user that game progress will be lost
                    adjustedGridSize = gridSize;
                    adjustedWinCombo = winCombo;
                    adjustedAI = ai;

                    gameController.startNewGame(adjustedGridSize, adjustedWinCombo, adjustedAI);
                }
                else if(adjustedAI != ai){
                    adjustedAI = ai;
                    gameController.setAiLevel(adjustedAI);
                }
            }
        }
    }

    @Override
    public void onLayoutChanged(Rect rcArea) {
        gameGrid.gridAreaChanged(rcArea);
    }

    @Override
    public void onUndoCountChanged(int count) {
        toast(String.valueOf(count));
    }

    public void toast(String msg){
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

}
