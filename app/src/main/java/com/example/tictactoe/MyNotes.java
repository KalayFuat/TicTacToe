package com.example.tictactoe;

import android.widget.SeekBar;

public class MyNotes {

/***** volume seekbar
SeekBar sb = findViewById(R.id.musicVolume);
        sb.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
               // sounds.setVolume((float)progress / 100);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

               <SeekBar
                android:id="@+id/musicVolume"
                android:layout_width="149dp"
                android:layout_height="wrap_content"
                android:layout_marginStart="8dp"
                android:layout_marginBottom="8dp"
                android:background="@drawable/image_border"
                android:max="255"
                android:progress="0"
                app:layout_constraintBottom_toTopOf="@id/spaceGridBottom"
                app:layout_constraintEnd_toStartOf="@+id/restartVolume"
                app:layout_constraintHorizontal_bias="0.5"
                app:layout_constraintStart_toStartOf="parent" />
 */

/***** grid-colour seekbar
SeekBar sb2 = findViewById(R.id.restartVolume);
        sb2.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
              //  sounds.sounds.setVolume(sounds.newGameId, (float)progress/100, (float)progress/100);
                setTitle(String.valueOf(progress));

                colour = colour & 0x00FFFFFF;//70
                colour = colour | (progress << 24);
                gameGrid.setCellBlankColour(colour);
                //
                // colour = colour & 0xFF00FFFF;//40
                // colour = colour | (progress << 16);
                // gameGrid.setCellBlankColour(colour);
                //
}

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
    }
});


     <SeekBar
     android:id="@+id/restartVolume"
     android:layout_width="143dp"
     android:layout_height="wrap_content"
     android:layout_marginEnd="8dp"
     android:layout_marginBottom="8dp"
     android:background="@drawable/image_border"
     android:max="255"
     android:progress="50"
     app:layout_constraintBottom_toTopOf="@id/spaceGridBottom"
     app:layout_constraintEnd_toEndOf="parent"
     app:layout_constraintHorizontal_bias="0.5"
     app:layout_constraintStart_toEndOf="@+id/musicVolume" />
 */
}
