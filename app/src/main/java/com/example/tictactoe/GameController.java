package com.example.tictactoe;

import android.graphics.Rect;
import android.util.Log;

import java.util.ArrayList;

import static com.example.tictactoe.GameGlobals.GAME_ENDED_DRAW;
import static com.example.tictactoe.GameGlobals.GAME_ENDED_O_WON;
import static com.example.tictactoe.GameGlobals.GAME_ENDED_X_WON;
import static com.example.tictactoe.GameGlobals.GAME_HUMAN_VS_AI;
import static com.example.tictactoe.GameGlobals.GAME_NOT_ENDED;
//import static com.example.newtictactoe.GameGlobals.PLAYER_AI;
//import static com.example.newtictactoe.GameGlobals.PLAYER_HUMAN;
import static com.example.tictactoe.GameGlobals.GAME_TAG;
import static com.example.tictactoe.GameGlobals.STATE_O;

public class GameController implements TicTacToePlayer.IPlayerPlayedTurn {

    private ArrayList<IPlayerTurn> IplayerTurn;
    private ArrayList<IWinComboCount> IwinComboCount;
    private ArrayList<IGrid> Igrid;
    private ArrayList<ICellInteraction> IcellInteraction;
    private ArrayList<IOpponent> Iopponent;
    private ArrayList<IGameEnded> IgameEnded;
    private ArrayList<INewRoundStarted> InewRoundStarted;
    private ArrayList<IComboChecker> IcomboChecker;
    private ArrayList<IUndoManager> IundoManager;
    private ArrayList<INewGameStarted> InewGameStarted;
    private ArrayList<IGamePauseResume> IgamePauseResume;
    private ArrayList<IUndoCountChanged> IundoCountChanged;
    private ArrayList<IGameIsQuitting> IgameIsQuitting;

    private boolean bOpponentPlaying;

    private GameModel model;
    private int iGameType;

    private boolean bAiActive;

    private LocalAIPlayer playerOpponent;

    public GameController(){
        init();
    }
/*
    public GameController(Context context){
        super(context);

        init();
    }

    public GameController(Context context, AttributeSet attrs) {
        super(context, attrs);

        init();
    }*/

    public interface IPlayerTurn{
        void displayPlayerTurn(int player);
    }

    public interface IWinComboCount{
        void displayWinComboCount(int count);
    }

    public interface IGrid{
        void setupNewGrid(Rect rcArea, int gridSize);
        void gridAreaChanged(Rect newArea);
    }

    public interface ICellInteraction{
        void cellClicked(SingleMove singleMove);
        void undoCell(SingleMove singleMove);
    }

    public interface IOpponent{
        SingleMove getNextMove(GameState gameState);
    }

    public interface IGameEnded {
        void gameEnded(GameState gameState);
        void roundEnded(GameState gameResult);
    }

    public interface IComboChecker{
        GameResult checkCombo(GameState gameState);
    }

    public interface IUndoManager{
        void addMove(int row, int col, int player);
        SingleMove getLastMove(int player);
    }

    public interface INewGameStarted{
        void onNewGameStarted(GameState gameState);
        void onFirstTimeLoad(GameState gameState);
    }

    public interface INewRoundStarted{
        void onNewRoundStarted(GameState gameState);
    }

    public interface IGamePauseResume{
        void onGamePaused(GameState gameState);
        void onGameResumed(GameState gameState);
    }

    public interface IUndoCountChanged{
        void onUndoCountChanged(int count);
    }

    public interface IGameIsQuitting{
        void onGameIsQuitting();
    }


    public void addIplayerTurn(IPlayerTurn iplayerTurn) {
        IplayerTurn.add(iplayerTurn);
    }

    public void addIwinComboCount(IWinComboCount iwinComboCount) {
        IwinComboCount.add(iwinComboCount);
    }

    public void addIgrid(IGrid igrid) {
        Igrid.add(igrid);
    }

    public void addIcellInteraction(ICellInteraction icellInteraction) {
        IcellInteraction.add(icellInteraction);
    }

    public void addIopponent(IOpponent iopponent) {
        Iopponent.add(iopponent);
    }

    public void addIgameEndedCelebration(IGameEnded igameEndedCelebration) {
        IgameEnded.add(igameEndedCelebration);
    }

    public void addIcomboChecker(IComboChecker icomboChecker) {
        IcomboChecker.add(icomboChecker);
    }

    public void addIundoManager(IUndoManager iundoManager) {
        IundoManager.add(iundoManager);
    }

    public void addInewGameStarted(INewGameStarted inewGameStarted) {
        InewGameStarted.add(inewGameStarted);
    }

    public void addInewRoundStarted(INewRoundStarted iNewRoundStarted){
        InewRoundStarted.add(iNewRoundStarted);
    }

    public void addIgamePauseResume(IGamePauseResume igamePauseResume){
        IgamePauseResume.add(igamePauseResume);
    }

    public void addIundoCountChanged(IUndoCountChanged iundoCountChanged){
        IundoCountChanged.add(iundoCountChanged);
    }

    public void addIgameIsQuitting(IGameIsQuitting igameIsQuitting){
        IgameIsQuitting.add(igameIsQuitting);
    }

    public void setModel(GameModel model){
        this.model = model;
    }

    private void init(){
        IplayerTurn = new ArrayList<>();
        IwinComboCount = new ArrayList<>();
        Igrid = new ArrayList<>();
        IcellInteraction = new ArrayList<>();
        Iopponent = new ArrayList<>();
        IgameEnded = new ArrayList<>();
        IcomboChecker = new ArrayList<>();
        IundoManager = new ArrayList<>();
        InewGameStarted = new ArrayList<>();
        InewRoundStarted = new ArrayList<>();
        IgamePauseResume = new ArrayList<>();
        IundoCountChanged = new ArrayList<>();
        IgameIsQuitting = new ArrayList<>();

        playerOpponent = new LocalAIPlayer();
        //playerOpponent.setPlayerType(PLAYER_AI);
        playerOpponent.setPlayer(GameGlobals.PLAYER_X);
        playerOpponent.setCallbackPlayTurn(this);

        iGameType = GAME_HUMAN_VS_AI;

        bAiActive = false;
    }

    public void onCLick_Settings(){

    }

    public void onClick_ToggleAI(){

    }

    public void onClick_Undo(){
        undoCell();
    }

    public void onClick_Reset(){

    }

    public void onClick_Back(){

    }

    private void onFirstTimeLoad(){
        GameState gs = model.getGameState();

        if(InewGameStarted != null) {
            for (INewGameStarted c : InewGameStarted) {
                c.onFirstTimeLoad(gs);
            }
        }
    }

    private void onStartNewGame(){
        GameState gs = model.getGameState();

        if(InewGameStarted != null) {
            for (INewGameStarted c : InewGameStarted) {
                c.onNewGameStarted(gs);
            }
        }
    }

    private void onStartNewRound(){
        GameState gs = model.getGameState();

        if(InewRoundStarted != null) {
            for (INewRoundStarted c : InewRoundStarted) {
                c.onNewRoundStarted(gs);
            }
        }
    }

    private void onWinComboCount(){
        GameState gs = model.getGameState();

        if(IwinComboCount != null) {
            for (IWinComboCount c : IwinComboCount) {
                c.displayWinComboCount(gs.winComboCount);
            }
        }
    }

    private void onCellClicked(SingleMove singleMove){
        if(IcellInteraction != null){
            for (ICellInteraction c : IcellInteraction) {
                c.cellClicked(singleMove);
            }
        }
    }

    private void onUndoCell(SingleMove singleMove){
        if(IcellInteraction != null){
            for (ICellInteraction c : IcellInteraction) {
                c.undoCell(singleMove);
            }
        }
    }

    private void onPlayerTurn(int player){
        if(IplayerTurn != null){
            for (IPlayerTurn c : IplayerTurn) {
                c.displayPlayerTurn(player);
            }
        }
    }

    private void onGameEndedCelebration(GameState gameState){
        if(IgameEnded != null){
            for (IGameEnded c : IgameEnded) {
                c.gameEnded(gameState);
            }
        }
    }

    private void onRoundEnded(GameState gameState){
        if(IgameEnded != null){
            for(IGameEnded c : IgameEnded){
                c.roundEnded(gameState);
            }
        }
    }

    private void onGamePause(GameState gameResult){
        if(IgamePauseResume != null){
            for (IGamePauseResume c : IgamePauseResume) {
                c.onGamePaused(gameResult);
            }
        }
    }

    private void onGameResume(GameState gameResult){
        if(IgamePauseResume != null){
            for (IGamePauseResume c : IgamePauseResume) {
                c.onGameResumed(gameResult);
            }
        }
    }

    private void onUndoCountChanged(int count){
        if(IundoCountChanged != null){
            for (IUndoCountChanged c : IundoCountChanged) {
                c.onUndoCountChanged(count);
            }
        }
    }

    private void onGameIsQuitting(){
        if(IgameIsQuitting != null){
            for(IGameIsQuitting c : IgameIsQuitting){
                c.onGameIsQuitting();
            }
        }
    }

    public void startNewRound(){
        bOpponentPlaying = false;

        if(model != null){
            model.startNewRound();

            //callbacks
            onStartNewRound();
            onPlayerTurn(model.getPlayerTurn());

            //TODO: maybe this can go in a new class 'players controller'
            if(model.getPlayerTurn() == playerOpponent.getPlayer()){
                startAiMove();
            }
        }
    }

    public void setAiLevel(int level){
        if(model != null){
            model.setiAILevel(level);
        }
    }

    private void cell_clicked(int row, int col, int player) {
        if (!isNextMovePlayable())
            return;

        if (model == null)
            return;

        GameResult result = model.playCell(row, col, player);
        if (result != null) {

            //notify others of cell clicked and accepted
            onCellClicked(new SingleMove(row, col, model.getPlayerTurn()));

            //check the state of the game
            switch (result.result) {
                case GAME_NOT_ENDED:

                    //game hasn't ended so it's next player's turn
                    nextPlayerTurn();

                    //TODO: this part should be in PlayerAi. nextPlayerTurn should notify PlayerAi of next player,
                    //TODO  then PlayerAi will catch this message, and call startAiMove.
                    //if next player is AI, then startAiMove()
                    if (model.getPlayerTurn() == playerOpponent.getPlayer() && iGameType == GAME_HUMAN_VS_AI) {
                        startAiMove();
                    } else if (model.getPlayerTurn() == STATE_O) {//users turn
                        //TODO: cant think of anything yet!!
                        //TODO: ideally players should be in an array and controlled by a dedicated class
                    }

                    break;
                case GAME_ENDED_X_WON:
                    xWon(model.getGameState());
                    break;
                case GAME_ENDED_O_WON:
                    oWon(model.getGameState());
                    break;
                case GAME_ENDED_DRAW:
                    gameDraw(model.getGameState());
                    break;
                default:
                    //displayError("Error on playCell() : default");
                    break;
            }
        }
    }

    private boolean isNextMovePlayable(){
        if(!model.hasGameStarted())
            return false;

        if(!model.hasRoundStarted())
            return false;

        if(model.hasRoundEnded())
            return false;

        return true;
    }

    //called from View (currently TictactoeGrid)
    public void cellClicked(int row, int col){
        cellClicked(row, col, model.getPlayerTurn());
    }

    //called from human click
    public void cellClicked(int row, int col, int player){
        if(!isNextMovePlayable())
            return;

        //if user has selected a cell
        if(player == STATE_O && !bOpponentPlaying){
            cell_clicked(row, col, STATE_O);
        }
        //else if user has selected a cell for opponent player (AI-off)
        else if(player == playerOpponent.getPlayer() && !bAiActive){
            cell_clicked(row, col, playerOpponent.getPlayer());
        }
    }

    //called from AI
    public void cellClickedFromOpponent(int row, int col){
        if(!isNextMovePlayable())
            return;

        if(!bOpponentPlaying)
            return;

        cell_clicked(row, col, playerOpponent.getPlayer());
    }

    private void nextPlayerTurn(){
        //update model
        model.nextPlayerTurn();

        //let others know about player turn
        onPlayerTurn(model.getPlayerTurn());
    }

    @Override
    public void onPlayerPlayedTurn(SingleMoveAIResult aiResult) {
        if(aiResult != null) {
           // Log.i(TAG, "GameController:onPlayerPlayedTurn: " + aiResult.player);
            //cellClicked(aiResult.row, aiResult.col, playerOpponent.getPlayer());
            cellClickedFromOpponent(aiResult.row, aiResult.col);
        }else
            Log.i(GAME_TAG, "GameController:onPlayerPlayedTurn: aiResult is null");
        bOpponentPlaying = false;
    }

    //TODO: this function should be in PlayerAi
    private void startAiMove(){
        if(!isNextMovePlayable())
            return;

        if(!bOpponentPlaying && bAiActive) {
            bOpponentPlaying = true;
            playerOpponent.getNextAIMove(model.getGameState());
        }
    }

    public void setAiActive(boolean state){
        bAiActive = state;

        if(bAiActive){
            if(model.getPlayerTurn() == playerOpponent.getPlayer() && iGameType == GAME_HUMAN_VS_AI){
                startAiMove();
            }
        }
    }

    //TODO: find a better way for model.moveCount() < 2
    public boolean undoCell(){
        if(model.hasRoundEnded() || model.hasGameEnded())
            return false;

        //if opponent is playing we have to wait for them
        //and also we can only undo minimum 2 moves, so return if less than 2;
        if(bOpponentPlaying || model.getMoveCount() < 2)
            return false;

        SingleUndoMove moves = model.undoCell();

        //TODO: moves.move1 or move2 can be null which makes app crash.
        if(moves != null && moves.move1 != null && moves.move2 != null) {
            onUndoCell(moves.move1);
            onUndoCell(moves.move2);

            //since we have undone 2 moves, its the same players turn and let everyone know
            onPlayerTurn(model.getPlayerTurn());

            //undo count has been changed, notify all
            onUndoCountChanged(model.undoManager.getMovesRemaining());

            //if opponents turn, then call them to make their next move
            if(model.getPlayerTurn() == playerOpponent.getPlayer()){
                startAiMove();
            }

            //return success of undo moves
            return true;
        }

        //return failure of moves-undone
        return false;
    }

    public void ContinueButton(String text){
        if(text.equals(MyApp.getContext().getResources().getString(R.string.new_game)))
            startNewGame();
        else
            startNewRound();
    }

    public void startNewGame(){
        startNewGame(model.getGridSize(), model.getWinComboCount(), model.getiAILevel());
    }

    public void startNewGame(int gridSize, int winComboCount, int difficulty){
        playerOpponent.abortMoveStarted();
        bOpponentPlaying = false;

        if(model != null){
            model.startNewGame(gridSize, winComboCount, difficulty);

            //callbacks
            onStartNewGame();
            onWinComboCount();
            onPlayerTurn(model.getPlayerTurn());

            //TODO: maybe this can go in a new class 'players controller'
            if(model.getPlayerTurn() == playerOpponent.getPlayer()){
                startAiMove();
            }
        }
    }

    public void firstTimeLoad(){
        onFirstTimeLoad();
    }

    public void resumeGame(){
        //if all these are false then its the first time opening a new game
        if(!model.hasGameStarted() && !model.hasGameEnded() && !model.hasRoundEnded()){
            //startNewGame();
            firstTimeLoad();
        }

        onGameResume(model.getGameState());
    }

    public void pauseGame(){
        onGamePause(model.getGameState());
    }

    private void xWon(GameState gameState){
        if(model.checkForWholeGameWinner()){
            onGameEndedCelebration(gameState);
        }
        else
            onRoundEnded(gameState);
    }

    private void oWon(GameState gameState){
        if(model.checkForWholeGameWinner()){
            onGameEndedCelebration(gameState);
        }
        else
            onRoundEnded(gameState);
    }

    private void gameDraw(GameState gameState){
        if(model.checkForWholeGameWinner()) {
            onGameEndedCelebration(gameState);
        }
        else
            onRoundEnded(gameState);
    }

    public void exitGame(){
        playerOpponent.abortMoveStarted();
        onGameIsQuitting();
    }
}