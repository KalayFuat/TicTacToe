package com.example.tictactoe;

import static com.example.tictactoe.GameGlobals.GAME_ENDED_O_WON;
import static com.example.tictactoe.GameGlobals.GAME_ENDED_X_WON;
import static com.example.tictactoe.GameGlobals.GAME_NOT_ENDED;
import static com.example.tictactoe.GameGlobals.PLAYER_O;
import static com.example.tictactoe.GameGlobals.PLAYER_X;
import static com.example.tictactoe.GameGlobals.STATE_UNUSED;

public class GameModel {
    private TictactoeBox[][] cells;
    private boolean bGameEnded;
    private boolean bGameStarted;
    private int gameWinComboCount;
    private int gameGridSize;
    private int moveCount;

    private int playerTurn;
    private int iAILevel;

    private int playerOScore;
    private int playerXScore;
    private int winningScore;
    private boolean bRoundEnded;

    private GameResult lastRoundResult;

    //////////////////////////////////////
    //TODO: not yet implemented
    private TicTacToePlayer playerMaster;
    private TicTacToePlayer playerOpponent;
    ///////////////////////////////////////

    UndoManager undoManager;

    public GameModel(){
        init();
    }

    private void init() {
        cells = null;//this is setup in startNewGame

        playerMaster = null;
        playerOpponent = null;

        bGameEnded = false;
        bGameStarted = false;
        bRoundEnded = false;

        gameWinComboCount = GameGlobals.DEFAULT_WIN_COMBO;
        gameGridSize = GameGlobals.DEFAULT_GRID_SIZE;
        moveCount = 0;

        playerTurn = GameGlobals.PLAYER_O;
        iAILevel = GameGlobals.DEFAULT_AI_DIFFICULTY;

        playerOScore = 0;
        playerXScore = 0;
        winningScore = GameGlobals.DEFAULT_GAME_WINNING_SCORE;

        lastRoundResult = null;

        undoManager = new UndoManager();
    }

    public TictactoeBox[][] getCells(){
        return cells;
    }

    public int getGridSize(){
        return gameGridSize;
    }

    public int getWinComboCount(){
        return gameWinComboCount;
    }

    public int getPlayerTurn(){
        return playerTurn;
    }

    private void setPlayerTurn(int newPlayer){
        playerTurn = newPlayer;
    }

    public int getiAILevel(){
        return iAILevel;
    }

    public void setiAILevel(int level){
        iAILevel = level;
    }

    public int getMoveCount(){
        return moveCount;
    }

    public TicTacToePlayer getPlayerMaster() {
        return playerMaster;
    }

    public void setPlayerMaster(TicTacToePlayer playerMaster) {
        this.playerMaster = playerMaster;
    }

    public TicTacToePlayer getPlayerOpponent() {
        return playerOpponent;
    }

    public void setPlayerOpponent(TicTacToePlayer playerOpponent) {
        this.playerOpponent = playerOpponent;
    }

    public int getPlayerOScore() {
        return playerOScore;
    }

    public void setPlayerOScore(int playerOScore) {
        this.playerOScore = playerOScore;
    }

    public int getPlayerXScore() {
        return playerXScore;
    }

    public void setPlayerXScore(int playerXScore) {
        this.playerXScore = playerXScore;
    }

    public int getWinningScore() {
        return winningScore;
    }

    public void setWinningScore(int winningScore) {
        this.winningScore = winningScore;
    }

    public GameResult getLastRoundResult() {
        return lastRoundResult;
    }

    public void setLastRoundResult(GameResult lastRoundResult) {
        this.lastRoundResult = lastRoundResult;
    }

    private void addPointToPlayer(int player){
        if(player  == GameGlobals.PLAYER_O)
            setPlayerOScore(getPlayerOScore() + 1);
        else if(player  == GameGlobals.PLAYER_X)
            setPlayerXScore(getPlayerXScore() + 1);
    }

    public void resetGame() {
        //TODO: timer.stop();

        undoManager.reset();

        if(cells != null) {
            resetCellsArray();
        }

        playerTurn = GameGlobals.PLAYER_NONE;
        playerOScore = 0;
        playerXScore = 0;
        bGameEnded = true;
        bGameStarted = false;
        bRoundEnded = false;
        moveCount = 0;
        lastRoundResult = null;
    }

    public void startNewGame(){
        startNewGame(getGridSize(), getWinComboCount(), iAILevel);
    }

    public void startNewGame(int gridSize, int numberOfWinBoxes, int aiLevel) {
        resetGame();

        playerTurn = GameGlobals.getRandomPlayer();

        iAILevel = aiLevel;
        gameGridSize = gridSize;
        gameWinComboCount = numberOfWinBoxes;

        setupCellsArray(gridSize, gridSize);

        bGameEnded = false;
        bGameStarted = true;
        bRoundEnded = false;
    }

    public void startNewRound(){
        undoManager.reset();

        if(cells != null) {
            resetCellsArray();
        }

        playerTurn = GameGlobals.getRandomPlayer();
        moveCount = 0;
        lastRoundResult = null;

        bRoundEnded = false;
    }

    private void setupCellsArray(int row_count, int col_count){
        cells = new TictactoeBox[row_count][col_count];

        for (int i = 0; i < row_count; i++) {
            for (int j = 0; j < col_count; j++) {
                TictactoeBox cell = new TictactoeBox(i, j);
                cell.setStateBlank();
                cells[i][j] = cell;
            }
        }

        //TODO: TictactoeAI should setup a callback (INewGameStarted) and call
        //TODO: initialiseGridString() itself.
        //call this so AI-Class does some preparation for changes
     //   TictactoeAI.initialiseGridString(row_count, col_count);
    }

    private void resetCellsArray(){
        for (TictactoeBox[] cell : cells) {
            for (int j = 0; j < cell.length; j++) {
                cell[j].reset();
            }
        }
    }

    public GameState getGameState(){
        GameState gs = new GameState();
        gs.cells = cells;
        gs.difficulty = getiAILevel();
        gs.gridSize = getGridSize();
        gs.winComboCount = getWinComboCount();
        gs.moveCount = getMoveCount();
        gs.playerOScore = getPlayerOScore();
        gs.playerXScore = getPlayerXScore();
        gs.lastRoundResult = getLastRoundResult();
        return gs;
    }

    public boolean hasGameStarted(){
        //return (moveCount > 0 && !bGameEnded);
        //return bGameStarted;//TODO: using this line gives the following bug: after a round is finished,
                              // user clicks on Settings then without making any changes they come back
                              // to the game screen, startNewGame() is called but it shouldn't because
                              // final score hasn't been reached and another few more rounds to play.

        //return (!bGameEnded && bGameStarted) || bRoundEnded;
        //TODO: using the above line gives the following bug: after a round is finished
        // (next round NOT started) user clicks on a cell and game gives another point to same user

        //return (!bGameEnded && bGameStarted);

        return bGameStarted;
    }

    public boolean hasGameEnded(){
        return bGameEnded;
    }

    public boolean hasRoundStarted(){
        return !bRoundEnded;
    }

    public boolean hasRoundEnded(){
        return bRoundEnded;
    }

    public SingleUndoMove undoCell(){
        if(!bGameEnded) {
            SingleUndoMove moves = undoManager.getLastMoves();
            if (moves.move1 != null) {
                cells[moves.move1.row][moves.move1.col].setStateBlank();

                //since the move is undone, it should be that players turn again
                playerTurn = moves.move1.player;

                //decrease moveCount as we went back once
                moveCount--;
            }

            if (moves.move2 != null) {
                cells[moves.move2.row][moves.move2.col].setStateBlank();

                //since the move is undone, it should be the current player's turn again
                playerTurn = moves.move2.player;

                //decrease moveCount as we went back once
                moveCount--;
            }

            return moves;
        }

        return null;
    }

    public GameResult playCell(int row, int col, int player) {
        if (!bGameEnded) {
            int temp = cells[row][col].getState();
            if (cells[row][col].getState() == STATE_UNUSED) {

                storeLastCellPlayed(row, col, player);

                cells[row][col].btnClicked(player);

                moveCount++;

                lastRoundResult = ComboChecker.checkIfWon(cells, getGridSize(), getGridSize(), getWinComboCount());

                //do we have a winner?
                if (lastRoundResult.result != GAME_NOT_ENDED) {
                    if(lastRoundResult.result == GAME_ENDED_O_WON){
                        addPointToPlayer(PLAYER_O);
                    }else if(lastRoundResult.result == GAME_ENDED_X_WON) {
                        addPointToPlayer(PLAYER_X);
                    }else{
                        //its a draw, don't add points to anyone
                    }

                    //check if a player has won the game, else they've only won this round
                    if(checkForWholeGameWinner()) {
                        roundEnded();
                        gameEnded();
                    }
                    else {
                        roundEnded();
                    }
                }
                else{
                    //TODO: caller should call nextPlayerTurn() to update playerTurn to next player
                }

                return lastRoundResult;
            }
        }

        return null;
    }

    public boolean checkForWholeGameWinner(){
        if(playerOScore >= winningScore || playerXScore >= winningScore)
            return true;
        else
            return false;
    }

    private void gameEnded(){
        bGameEnded = true;
        bGameStarted = false;
    }

    private void roundEnded(){
        bRoundEnded = true;
    }

    private void roundStarted(){
        bRoundEnded = false;
    }

    private void storeLastCellPlayed(int row, int col, int player){
        undoManager.addMove(row, col, player);
    }

    public void nextPlayerTurn(){
        setPlayerTurn(GameGlobals.invertPlayer(playerTurn));
    }
}
