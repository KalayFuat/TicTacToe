package com.example.tictactoe;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.constraintlayout.widget.Guideline;

public class GameResultDisplay extends RelativeLayout implements GameController.INewGameStarted,
                                                                 GameController.INewRoundStarted,
                                                                 GameController.IGameEnded {

    MyTimer timerHideWhoWon;
    MyTimer timerShowGuideButton;

    public GameResultDisplay(Context context) {
        super(context);

        init();
    }

    public GameResultDisplay(Context context, AttributeSet attrs) {
        super(context, attrs);

        init();
    }

    public void init(){
        timerHideWhoWon = new MyTimer(new MyTimer.ITimerElapsed() {
            @Override
            public void onTimerElapsed(long timeNow) {
                hideResultsDisplay();
                timerShowGuideButton.start();
            }
        }, 4000, false);

        timerShowGuideButton = new MyTimer(new MyTimer.ITimerElapsed() {
            @Override
            public void onTimerElapsed(long timeNow) {
                hideResultsDisplay();//to make sure its hidden
                ((ButtonGuide)findViewById(R.id.buttonNextRound)).setText(
                        getResources().getString(R.string.new_game)
                );
                showContinueButton();
            }
        }, 1250, false);
    }

    public void showContinueButton(){
        setAlpha(0.0f);
        ((ButtonGuide)findViewById(R.id.buttonNextRound)).setVisibility(VISIBLE);
        animate().alpha(1.0f).setDuration(500).start();
    }

    public void hideContinueButton() {
        ((ButtonGuide)findViewById(R.id.buttonNextRound)).setVisibility(INVISIBLE);
    }

    public void hideResultsDisplay(){
        findViewById(R.id.textGameResult).setVisibility(View.INVISIBLE);
    }

    public void showResultsDisplay(){
        setAlpha(0.0f);
        findViewById(R.id.textGameResult).setVisibility(View.VISIBLE);
        animate().alpha(1.0f).setDuration(1000).start();
        timerHideWhoWon.start();
    }

    public void showResultDisplay(GameState gameState){
        String winnerText;

        if(gameState.playerOScore > gameState.playerXScore) {
            winnerText = getResources().getString(R.string.o_won);
        }else if(gameState.playerOScore < gameState.playerXScore){
            winnerText = getResources().getString(R.string.x_won);
        }else{
            winnerText = getResources().getString(R.string.ox_draw);
        }

        ((TextView)findViewById(R.id.textGameResult)).setText(winnerText);
        showResultsDisplay();
    }

    @Override
    public void gameEnded(GameState gameState) {
        hideContinueButton();
        showResultDisplay(gameState);
    }

    @Override
    public void roundEnded(GameState gameResult) {
        ButtonGuide btnGuide = ((ButtonGuide)findViewById(R.id.buttonNextRound));
        btnGuide.setText(getResources().getString(R.string.button_continue));
        showContinueButton();
    }

    @Override
    public void onNewGameStarted(GameState gameState) {
        hideContinueButton();
        hideResultsDisplay();
    }

    @Override
    public void onFirstTimeLoad(GameState gameState) {
        ButtonGuide btnGuide = ((ButtonGuide)findViewById(R.id.buttonNextRound));
        btnGuide.setText(getResources().getString(R.string.new_game));
        btnGuide.setVisibility(View.VISIBLE);
    }

    @Override
    public void onNewRoundStarted(GameState gameState) {
        hideContinueButton();
    }
}
