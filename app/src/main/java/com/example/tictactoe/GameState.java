package com.example.tictactoe;

public class GameState {
    TictactoeBox[][] cells;
    int difficulty;
    int gridSize;
    int winComboCount;
    int moveCount;
    int playerOScore;
    int playerXScore;
    GameResult lastRoundResult;
    boolean bSoundOn;

    public void reset(){
        cells = null;
        difficulty = 0;
        gridSize = 0;
        winComboCount = 0;
        moveCount = 0;
        playerOScore = 0;
        playerXScore = 0;
        lastRoundResult = null;
        bSoundOn = false;
    }
}