package com.example.tictactoe;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;

public class ButtonGuide extends androidx.appcompat.widget.AppCompatButton {
    enum GameButtonStates{
        NEW_GAME,
        CONTINUE,
    }

    public ButtonGuide(Context context) {
        super(context);
    }

    public ButtonGuide(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setState(GameButtonStates state){
        if(state == GameButtonStates.NEW_GAME)
            this.setText(getResources().getString(R.string.new_game));
        else if(state == GameButtonStates.CONTINUE)
            this.setText(getResources().getString(R.string.button_continue));
    }

    public void hideButton(){
        setVisibility(INVISIBLE);
    }

    public void showButton(){
        setVisibility(VISIBLE);
    }
}
