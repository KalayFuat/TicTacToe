package com.example.tictactoe;

import static com.example.tictactoe.GameGlobals.GAME_TAG;

import android.annotation.TargetApi;
import android.content.Context;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class SoundEffects implements Serializable,
                                     GameController.ICellInteraction,
                                     GameController.IGameEnded,
                                     GameController.INewGameStarted,
                                     GameController.IGameIsQuitting,
                                     GameController.IGamePauseResume,
                                     GameController.INewRoundStarted,
                                     MyTimer.ITimerElapsed{

    enum Fading{
        FADE_IN,
        FADE_OUT
    }

    class ThreadExitHelper{
        boolean bExitThread = false;
    }

    private static final int SONG_CHANGE_DELAY_MILLIS = 20000;

    private static final int[] AMBIENT_SONGS = {R.raw.music_zapsplat_atmospheric_tension, R.raw.ambient};
    private static final int[] SOUND_FX = {R.raw.new_game,
                                           R.raw.pop1,
                                           R.raw.pop2,
                                           R.raw.player_win,
                                           R.raw.player_win,
                                           R.raw.undo_move};

    private static final String[] SOUND_FX_DESC = {"new_game",
                                                "playerO_play",
                                                "playerX_play",
                                                "playerO_won",
                                                "playerX_won",
                                                "undo_move",};

    private final HashMap<Integer, Boolean> mapSoundsLoadedFlags;
    private final HashMap<String, Integer> mapSoundsIDs;

    Context context;

    // Set to the volume of the MediaPlayer
    float volume = 1;
    float ambientVolume = 0.5f;
    float soundsVolume = 0.3f;

    private SoundPool sounds;
    public MediaPlayer mediaPlayer;

    final ThreadExitHelper threadExitHelper;

    private boolean bFirst = true;
    MyTimer timerNewSongDuration;//when timer elapses, startFadeInOut(false) is called
    private int currentSongIndex;
    private boolean bFadeOut;

    public SoundEffects(Context context){
        this.context = context;

        threadExitHelper = new ThreadExitHelper();
        threadExitHelper.bExitThread = false;

        createSoundPool();

        timerNewSongDuration = new MyTimer(this, SONG_CHANGE_DELAY_MILLIS, false);

        mapSoundsLoadedFlags = new HashMap<>();
        mapSoundsIDs = new HashMap<>();

        for(int i=0 ; i < SOUND_FX.length; i++){
            int id = sounds.load(context, SOUND_FX[i], 1);
            mapSoundsLoadedFlags.put(id, false);
            mapSoundsIDs.put(SOUND_FX_DESC[i], id);
        }
        sounds.setOnLoadCompleteListener((soundPool, sampleId, status) -> {
            mapSoundsLoadedFlags.put(sampleId, status == 0);
            Log.i("sounds", "SoundEffects: sound loaded: "
                                     + mapSoundsIDs.keySet().toArray()[sampleId-1]
                                     + " : flag: " + (status == 0));
          /*
            if(mapSoundsIDs.keySet().toArray()[sampleId-1].equals("new_game")) {
                if(!bFirst){
                    playSound("new_game");
                }
            }
            */

        });

        bFadeOut = false;
        currentSongIndex = (new Random()).nextInt(AMBIENT_SONGS.length);
        createMediaPlayerAndPlayAmbientMusic(AMBIENT_SONGS[currentSongIndex], 0.05f);
        startFadeInOut(false);

        timerNewSongDuration.start();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    protected void createSoundPool() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            createNewSoundPool();
        } else {
            createOldSoundPool();
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    protected void createNewSoundPool(){
        AudioAttributes attributes = new AudioAttributes.Builder()
                .setUsage(AudioAttributes.USAGE_MEDIA)
                .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                .build();
        sounds = new SoundPool.Builder()
                .setAudioAttributes(attributes)
                .setMaxStreams(5)
                .build();
    }

    protected void createOldSoundPool(){
        sounds = new SoundPool(5,AudioManager.STREAM_MUSIC,0);
    }

    public void setVolume(float volume){
        mediaPlayer.setVolume(volume, volume);
    }

    public void playSound(String soundName){
        playSound(mapSoundsIDs.get(soundName));
    }

    public void playSound(int sampleId){
        if(sounds == null)
            return;

        //Boolean.TRUE is used to prevent crash if NullPointerException is raised
        if((Boolean.TRUE.equals(mapSoundsLoadedFlags.get(sampleId))))
            sounds.play(sampleId, soundsVolume, soundsVolume, 0, 0, 1);
    }

    public void playGameEndedSound(GameState gameState){
        playRoundEndedSound(gameState);
    }

    public void playRoundEndedSound(GameState gameState){
        if (gameState.lastRoundResult.result == GameGlobals.GAME_ENDED_O_WON){
            playSound("playerO_won");
        }else if(gameState.lastRoundResult.result == GameGlobals.GAME_ENDED_X_WON){
            playSound("playerX_won");
        }
    }

    //TODO:remove bFirst and its block as music is started when this class is created
    public void playNewRoundStarted(GameState gameState) {
        playSound("new_game");
    }

    public void playNewGameStarted(GameState gameState){
   //     if(bFirst) {
        //    mediaPlayer.setVolume(ambientVolume, ambientVolume);
        //    mediaPlayer.start();
            bFirst = false;
   //     }else{
           playSound("new_game");
    //    }
    }

    public void pauseSounds(){
        threadExitHelper.bExitThread = true;
        timerNewSongDuration.stop();

        mediaPlayer.pause();
    }

    public void continuePlaying(){
        mediaPlayer.start();
    }

    @Override
    public void gameEnded(GameState gameState) {
        playGameEndedSound(gameState);
    }

    @Override
    public void roundEnded(GameState gameState) {
        playRoundEndedSound(gameState);
    }

    @Override
    public void onNewRoundStarted(GameState gameState) {
        playNewRoundStarted(gameState);
    }

    @Override
    public void onNewGameStarted(GameState gameState) {
        Log.i(GAME_TAG, "SoundEffects:onNewGameStarted: ");
        playNewGameStarted(gameState);
    }

    @Override
    public void onFirstTimeLoad(GameState gameState) {

    }

    @Override
    public void onGameIsQuitting() {
        Log.i(GAME_TAG, "onGameIsQuitting: ");
        releaseResources();
    }

    @Override
    public void onGamePaused(GameState gameState) {
        Log.i(GAME_TAG, "SoundEffetcs:onGamePaused: ");
        pauseSounds();
    }

    @Override
    public void onGameResumed(GameState gameState) {
        continuePlaying();
    }

    @Override
    public void onTimerElapsed(long timeNow) {
        startFadeInOut(bFadeOut);
    }

    @Override
    public void cellClicked(SingleMove singleMove) {
        if(sounds == null){
            Log.e(GAME_TAG, "SoundEffects:cellClicked: sounds == null");
            return;
        }

        if (singleMove.player == GameGlobals.PLAYER_O){
            playSound("playerO_play");
        }else if(singleMove.player == GameGlobals.PLAYER_X){
            playSound("playerX_play");
        }
    }

    @Override
    public void undoCell(SingleMove singleMove) {
        if (sounds == null) {
            Log.e(GAME_TAG, "SoundEffects:undoCell: sounds == null");
            return;
        }

        playSound("undo_move");
    }

    //How this works:
    //when timerNewSongDuration elapses, it calls this function. then..
    //in run(), 'volume' is increased/decreased until reached target (0 or ambientVolume),
    //then the next song is played within run(), timerNewSongDuration is fired to repeat the process,
    //and finally the run() task is ended
    //bFadeOut==true then fade out, else fade in
    private void startFadeInOut(boolean bFadeOut)
    {
        // The duration of the fade
        final int FADE_DURATION = 5000;

        // The amount of time between volume changes. The smaller this is, the smoother the fade
        final int FADE_INTERVAL = 1;

        // Calculate the number of fade steps
        int numberOfSteps = FADE_DURATION / FADE_INTERVAL;

        //'volume' will start from 'ambientVolume' counting down to 0 or..
        //'volume' will start from 0 counting up
        volume = (bFadeOut) ? ambientVolume : 0;

        // Calculate by how much the volume changes each step
        // also take into account fade-in or fade-out
        final float deltaVolume = (bFadeOut) ? volume / numberOfSteps : -(ambientVolume / numberOfSteps);

        // Create a new Timer and Timer task to run the fading outside the main UI thread
        final Timer timer = new Timer(true);
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                //Log.i(GAME_TAG, "run: Threads: " + String.valueOf(Thread.activeCount()));

                if(threadExitHelper.bExitThread) {
                    Log.i(GAME_TAG, "run: bExitThread is TRUE");
                    this.cancel();
                    return;
                }

                //Do a fade step
                fadeStep(deltaVolume);

                //if fading-out
                if (bFadeOut) {
                    if (volume <= 0) {
                        //Cancel and Purge the Timer if the desired volume has been reached
                        timer.cancel();
                        timer.purge();

                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                        if(threadExitHelper.bExitThread){
                            this.cancel();//stops execution of this task
                            return;
                        }

                        synchronized (threadExitHelper) {
                            currentSongIndex = getNextSong();
                            createMediaPlayerAndPlayAmbientMusic(AMBIENT_SONGS[currentSongIndex], 0.05f);
                            SoundEffects.this.bFadeOut = false;
                            timerNewSongDuration.setCallbackTime(200);
                            timerNewSongDuration.start();
                        }
                    }
                } else {//else fading-in
                    if (volume >= ambientVolume) {
                        //Cancel and Purge the Timer if the desired volume has been reached
                        timer.cancel();
                        timer.purge();

                        if(threadExitHelper.bExitThread){
                            this.cancel();//stops execution of this task
                            return;
                        }

                        synchronized (threadExitHelper) {
                            SoundEffects.this.bFadeOut = true;
                            timerNewSongDuration.setCallbackTime(SONG_CHANGE_DELAY_MILLIS);
                            timerNewSongDuration.start();
                        }
                    }
                }
            }
        };

        timer.schedule(timerTask, FADE_INTERVAL, FADE_INTERVAL);
    }

    private void fadeStep(float deltaVolume){
        if(threadExitHelper.bExitThread){
            Log.i(GAME_TAG, "fadeStep: bExitThread is true");
            return;
        }

        synchronized (threadExitHelper) {
            volume -= deltaVolume;

            //we need this 'if' statement incase mediaPlayer.release() is called during 'fading'
            if (!threadExitHelper.bExitThread && mediaPlayer != null) {
                if (mediaPlayer.isPlaying()) {
                    mediaPlayer.setVolume(volume, volume);
                }
            }
        }
    }

    //function only gets next element from array. Does NOT update currentSong
    private int getNextSong(){
        if(currentSongIndex + 1 >= AMBIENT_SONGS.length){
            return 0;
        }
        else
            return currentSongIndex + 1;
    }

    private void createMediaPlayerAndPlayAmbientMusic(int id, float volume){
        if(threadExitHelper.bExitThread)
            return;

        releaseMediaPlayerResources(mediaPlayer);

        mediaPlayer = MediaPlayer.create(MyApp.getContext(), id);
        mediaPlayer.setVolume(volume, volume);
        mediaPlayer.setLooping(true);
        mediaPlayer.start();

        threadExitHelper.bExitThread = false;
    }

    private void stopAmbientMusic(){
        synchronized (threadExitHelper) {
            if (mediaPlayer != null) {
                if (mediaPlayer.isPlaying())
                    mediaPlayer.stop();
            }
        }
    }

    public void releaseResources(){
            threadExitHelper.bExitThread = true;
            timerNewSongDuration.stop();

            sounds.release();
            sounds = null;

            releaseMediaPlayerResources(mediaPlayer);
            mediaPlayer = null;
    }

    private void releaseMediaPlayerResources(MediaPlayer media){
        //synchronized (threadExitHelper) {
            threadExitHelper.bExitThread = true;
            timerNewSongDuration.stop();

            if (media != null) {
                if (media.isPlaying()) {
                    media.stop();
                }
                media.reset();
                media.release();
            }
       // }
    }
}

/*
        private void startFadeInOut(boolean bFadeOut){
        if(bFadeOut)
            //'volume' will be counting down from 'ambientVolume'
            volume = ambientVolume;
        else
            //'volume' will start from 0 counting up
            volume = 0;

        // The duration of the fade
        final int FADE_DURATION = 5000;

        // The amount of time between volume changes. The smaller this is, the smoother the fade
        final int FADE_INTERVAL = 1;

        // Calculate the number of fade steps
        int numberOfSteps = FADE_DURATION / FADE_INTERVAL;

        // Calculate by how much the volume changes each step
        // also take into account fade-in or fade-out
        final float deltaVolume = (bFadeOut) ? volume / numberOfSteps : -(ambientVolume / numberOfSteps);

        // Create a new Timer and Timer task to run the fading outside the main UI thread
        final Timer timer = new Timer(true);
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                Log.i(TAG, "run: Threads: " + String.valueOf(Thread.activeCount()));

                //Do a fade step
                fadeStep(deltaVolume);

                //Cancel and Purge the Timer if the desired volume has been reached
                if(bFadeOut){
                    if(volume <= 0){
                        timer.cancel();
                        timer.purge();

                        try {
                            Thread.sleep(2000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                        currentSong = getNextSong();
                        createMediaPlayerAndPlayAmbientMusic(AMBIENT_SONGS[currentSong], 0.05f);
                        startFadeInOut(false);
                    }
                }
                else{
                    if(volume >= ambientVolume){
                        timer.cancel();
                        timer.purge();

                        timerNewSongDuration.start();
                    }
                }

            }
        };

        timer.schedule(timerTask,FADE_INTERVAL,FADE_INTERVAL);
    }
 */